package com.mauricioBatlle.services.cryptservice.configuration;

import java.util.Collections;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
	@Value("${debug}")
	private Boolean swaggerEnabled;

	private ApiInfo TelusApiInfo() {
	     return new ApiInfo(
	       "Mauricio Batlle's Backend Cryptography Service", 
	       "API that provides AES256 Text and File Encryption", 
	       "", 
	       "", 
	       new Contact("Mauricio Batlle", "https://www.linkedin.com/in/mauricioabatlle/", "mauricioacastro@gmail.com"), 
	       "", "", Collections.emptyList());
	}
	
    @Bean
    public Docket api() {
    	if(swaggerEnabled) {
            return new Docket(DocumentationType.SWAGGER_2)  
                    .select()                                  
                    .apis(RequestHandlerSelectors.any())              
                    .paths(PathSelectors.any())                          
                    .build().apiInfo(TelusApiInfo());  
    		
    	}
    	else 
            return new Docket(DocumentationType.SWAGGER_2)  
                    .select()                                  
                    .apis(RequestHandlerSelectors.none())              
                    .paths(PathSelectors.none())                          
                    .build().apiInfo(TelusApiInfo());  
                                         
    }

}
