package com.mauricioBatlle.services.cryptservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.ComponentScan;
import org.stagemonitor.core.Stagemonitor;
import org.springframework.boot.web.support.SpringBootServletInitializer;
@SpringBootApplication
@ComponentScan(basePackages= {"com.mauricioBatlle.services.cryptservice.configuration","com.mauricioBatlle.services.cryptservice.controllers","com.mauricioBatlle.services.cryptservice.helpers","com.mauricioBatlle.services.cryptservice.pojos"})
public class CryptServiceApplication extends SpringBootServletInitializer {
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(CryptServiceApplication.class);
    }
	public static void main(String[] args) {
		Stagemonitor.init();
		SpringApplication.run(CryptServiceApplication.class, args);
	}
}
