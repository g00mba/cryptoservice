package com.mauricioBatlle.services.cryptservice.controllers;

import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mauricioBatlle.services.cryptservice.configuration.BadRequestException;
import com.mauricioBatlle.services.cryptservice.configuration.InternalServerErrorException;
import com.mauricioBatlle.services.cryptservice.controllers.interfaces.BaseControllerInterface;
import com.mauricioBatlle.services.cryptservice.helpers.AppHelper;
import com.mauricioBatlle.services.cryptservice.pojos.requests.CryptoFileRequestPojo;
import com.mauricioBatlle.services.cryptservice.pojos.requests.CryptoMetaFileRequestPojo;
import com.mauricioBatlle.services.cryptservice.pojos.requests.CryptoMetaTextRequestPojo;
import com.mauricioBatlle.services.cryptservice.pojos.requests.CryptoTextRequestPojo;
import com.mauricioBatlle.services.cryptservice.pojos.requests.PlainFileRequestPojo;
import com.mauricioBatlle.services.cryptservice.pojos.requests.PlainTextRequestPojo;
import com.mauricioBatlle.services.cryptservice.pojos.responses.CryptoFileResponsePojo;
import com.mauricioBatlle.services.cryptservice.pojos.responses.CryptoTextResponsePojo;
import com.mauricioBatlle.services.cryptservice.pojos.responses.PlainFileResponsePojo;
import com.mauricioBatlle.services.cryptservice.pojos.responses.PlainTextResponsePojo;

@RestController
public class CryptoController implements BaseControllerInterface {
	static final org.slf4j.Logger controllerLogger = LoggerFactory.getLogger(CryptoController.class);
	static String errorInternalStr = "500 - Internal Server Error: ";

	@RequestMapping(value = "/encrypt-file", method = RequestMethod.POST)
	@Override
	public CryptoFileResponsePojo encryptFilePath(@RequestBody CryptoFileRequestPojo request) {
		controllerLogger.info("Begin file encryption rest path");
		controllerLogger.debug("Request data: "+request.getContent());
		CryptoFileResponsePojo responseHelper;
		byte[] plainFile;
		try {
			plainFile = request.getContent();
			plainFile.getClass();
		} catch (Exception e1) {
			String exE1="The 'fileString' parameter is missing";
			controllerLogger.error(exE1);
			throw new BadRequestException(exE1);
		}
		if (plainFile.length <= 1) {
			String exLength="The 'fileString' parameter must not be empty or smaller than 1 byte";
			controllerLogger.error(exLength);
			throw new BadRequestException(exLength);
		}
		try {
			AppHelper helper = new AppHelper();
			responseHelper = helper.encodeFile(plainFile);
		} catch (Exception e) {
			controllerLogger.error("could not encrypt file: " + e.getMessage());
			throw new InternalServerErrorException(errorInternalStr + e.getMessage());
		}
		controllerLogger.info("File encryption rest path finished");
		return responseHelper;
	}
	
	@RequestMapping(value = "/encrypt-meta-file", method = RequestMethod.POST)
	@Override
	public CryptoFileResponsePojo encryptMetaFilePath(@RequestBody CryptoMetaFileRequestPojo request) {
		controllerLogger.info("Begin file encryption rest path");
		controllerLogger.debug("Request data: "+request.getContent());
		CryptoFileResponsePojo responseHelper;
		byte[] plainFile;
		String salt;
		String password;
		try {
			
			plainFile = request.getContent();
			plainFile.getClass();
			salt=request.getSalt();
			password=request.getPassword();
			
		} catch (Exception e1) {
			String exE1="The request has missing parameters";
			controllerLogger.error(exE1);
			throw new BadRequestException(exE1);
		}
		if (plainFile.length <= 1) {
			String exLength="The 'fileString' parameter must not be empty or smaller than 1 byte";
			controllerLogger.error(exLength);
			throw new BadRequestException(exLength);
		}
		if (salt.length() < 8) {
			String exLength="The 'salt' parameter must not be empty or less than 8 characters";
			controllerLogger.error(exLength);
			throw new BadRequestException(exLength);
		}
		if (password.length() < 8) {
			String exLength="The 'password' parameter must not be empty or less than 8 characters";
			controllerLogger.error(exLength);
			throw new BadRequestException(exLength);
		}
		
		try {
			AppHelper helper = new AppHelper();
			responseHelper = helper.encodeFile(plainFile,salt,password);
		} catch (Exception e) {
			controllerLogger.error("could not encrypt file: " + e.getMessage());
			throw new InternalServerErrorException(errorInternalStr + e.getMessage());
		}
		controllerLogger.info("File encryption rest path finished");
		return responseHelper;
		
	}
	
	@RequestMapping(value = "/encrypt-plain-text", method = RequestMethod.POST)
	@Override
	public CryptoTextResponsePojo encryptTextPath(@RequestBody CryptoTextRequestPojo request) {
		controllerLogger.info("begin text encryption rest path");
		controllerLogger.debug("encryption request content: "+request.getContent());
		
		CryptoTextResponsePojo responseHelper;
		if (request.getContent()==null || request.getContent().isEmpty()) {
			String exEmpty="The 'textString' parameter must not be null or empty";
			controllerLogger.error(exEmpty);
			throw new BadRequestException(exEmpty);
		}
		String plainText = request.getContent();
		try {
			AppHelper helper = new AppHelper();
			responseHelper = helper.encodeText(plainText);
		} catch (Exception e) {
			controllerLogger.error("could not encrypt text: " + e.getMessage());
			throw new InternalServerErrorException(errorInternalStr + e.getMessage());
		}
		controllerLogger.info("Text encryption rest path finished");

		return responseHelper;
	}
		
	@RequestMapping(value = "/encrypt-meta-text", method = RequestMethod.POST)
	@Override	
	public CryptoTextResponsePojo encryptMetaTextPath(@RequestBody CryptoMetaTextRequestPojo request) {

		controllerLogger.info("begin text encryption rest path");
		controllerLogger.info("Salt and password provided");
		controllerLogger.debug("encryption request content: "+request.getContent());
		
		CryptoTextResponsePojo responseHelper;
		if (request.getContent()==null || request.getContent().isEmpty()
				||request.getSalt()==null||request.getSalt().isEmpty()
				||request.getPassword()==null||request.getPassword().isEmpty()
				) {
			String exEmpty="Parameters must not be null or empty";
			controllerLogger.error(exEmpty);
			throw new BadRequestException(exEmpty);
		}
		String plainText = request.getContent();
		String salt=request.getSalt();
		String password=request.getPassword();
		try {
			AppHelper helper = new AppHelper();
			responseHelper = helper.encodeText(plainText, salt, password);
		} catch (Exception e) {
			controllerLogger.error("could not encrypt text: " + e.getMessage());
			throw new InternalServerErrorException(errorInternalStr + e.getMessage());
		}
		controllerLogger.info("Text encryption rest path finished");

		return responseHelper;
		
	}
	
	@RequestMapping(value = "/decrypt-file", method = RequestMethod.POST)
	@Override
	public PlainFileResponsePojo decryptFilePath(@RequestBody PlainFileRequestPojo request) {

		controllerLogger.info("begin file decryption rest path");
		if (request.getSalt().equals("") || request.getSalt().isEmpty()) {
			String  emptySaltEx="The 'salt' parameter must not be null or empty";
			controllerLogger.error(emptySaltEx);
			throw new BadRequestException(emptySaltEx);
		}
		if (request.getPassword().equals("") || request.getPassword().isEmpty()) {
			String emptyPwdEx="The 'password' parameter must not be null or empty";
			controllerLogger.error(emptyPwdEx);
			throw new BadRequestException(emptyPwdEx);
		}
		if (request.getCrc().equals("") || request.getCrc().isEmpty()) {
			String emptyCrcEx="The 'crc' parameter must not be null or empty";
			controllerLogger.error(emptyCrcEx);
			throw new BadRequestException(emptyCrcEx);
		}
		if (request.getCryptCrc().equals("") || request.getCryptCrc().isEmpty()) {
			String emptyCryptCrcEx="The 'cryptCrc' parameter must not be null or empty";
			controllerLogger.error(emptyCryptCrcEx);
			throw new BadRequestException(emptyCryptCrcEx);
		}
		try {
			request.getFileString().getClass();
			if (request.getFileString().length<=1) {
				String emptyCryptoFileEx="The 'cryptoFile' parameter must not be empty";
				controllerLogger.error(emptyCryptoFileEx);
				throw new BadRequestException(emptyCryptoFileEx);
			}
		} catch (Exception e1) {
			String emptyFileEx="The 'fileString' is missing";
			controllerLogger.error(emptyFileEx);
			throw new BadRequestException(emptyFileEx);
		}
		final String salt=request.getSalt();
		final String crc=request.getCrc();
		final String cryptCrc=request.getCryptCrc();
		final String password=request.getPassword();
		final byte[] cryptoFile=request.getFileString();
		controllerLogger.debug(
				" salt: "+salt
				+" crc: "+crc
				+" pwd: "+password
				+" cryptoFile: "+cryptoFile
				);
		
		PlainFileResponsePojo responseHelper;

		try {
			AppHelper helper = new AppHelper();

			responseHelper = helper.decodeBinary(salt, password, crc, cryptCrc, cryptoFile);

		} catch (Exception e) {
			controllerLogger.error("could not decrypt file: " + e.getMessage());
			throw new InternalServerErrorException(errorInternalStr + e.getMessage());
		}

		controllerLogger.info("File decryption rest path finished");

		return responseHelper;
	}

	@RequestMapping(value = "/decrypt-plain-text", method = RequestMethod.POST)
	@Override
	public PlainTextResponsePojo decryptTextPath(@RequestBody PlainTextRequestPojo request) {
		controllerLogger.info("begin text decryption rest path");
		if (request.getSalt()==null || request.getSalt().isEmpty()) {
			String emptySaltEx = "The 'salt' parameter must not be null or empty";
			controllerLogger.error("The 'salt' parameter must not be null or empty");
			throw new BadRequestException(emptySaltEx);
		}
		if (request.getPassword()==null|| request.getPassword().isEmpty()) {
			String emptyPwdEx="The 'password' parameter must not be null or empty";
			controllerLogger.error(emptyPwdEx);
			throw new BadRequestException(emptyPwdEx);
		}
		if (request.getCrc()==null || request.getCrc().isEmpty()) {
			String emptyCrcEx="The 'crc' parameter must not be null or empty";
			controllerLogger.error(emptyCrcEx);
			throw new BadRequestException(emptyCrcEx);
		}
		if (request.getCryptCrc()==null || request.getCryptCrc().isEmpty()) {
			String emptyCryptCrcEx="The 'cryptCrc' parameter must not be null or empty";
			controllerLogger.error(emptyCryptCrcEx);
			throw new BadRequestException(emptyCryptCrcEx);
		}
		if (request.getTextString()==null || request.getTextString().isEmpty()) {
			String emptyTextEx="The 'textString' parameter must not be null or empty";
			controllerLogger.error(emptyTextEx);
			throw new BadRequestException(emptyTextEx);
		}
		final String salt=request.getSalt();
		final String password=request.getPassword();
		final String crc=request.getCrc();
		final String cryptCrc=request.getCryptCrc();
		final String cryptoText=request.getTextString();
		controllerLogger.debug(" salt: "+salt
				+" pwd: "+password
				+" crc: "+crc
				+" cryptcrc: "+cryptCrc
				+" cryptoText: "+cryptoText
				);
		
		PlainTextResponsePojo responseHelper;

		try {
			AppHelper helper = new AppHelper();
			responseHelper = helper.decodeText(salt, password, crc, cryptCrc, cryptoText);
		} catch (Exception e) {
			controllerLogger.error("could not decrypt text: " + e.getMessage());
			throw new InternalServerErrorException(errorInternalStr + e.getMessage());
		}
		controllerLogger.info("Text decryption rest path finished");
		

		return responseHelper;
	}

}
