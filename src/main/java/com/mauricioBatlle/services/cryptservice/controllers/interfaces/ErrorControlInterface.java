package com.mauricioBatlle.services.cryptservice.controllers.interfaces;

public interface ErrorControlInterface {

	String getErrorPath();

}