package com.mauricioBatlle.services.cryptservice.controllers;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.web.ErrorAttributes;
import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.mauricioBatlle.services.cryptservice.controllers.interfaces.ErrorControlInterface;

@RestController
public class CryptoErrorController implements ErrorController, ErrorControlInterface {
	private static final String PATH="/error";
	@Value("${debug}")
	private boolean debug;
	
	@Autowired
	private ErrorAttributes errorAttributes;
	
	@RequestMapping(value=PATH)
	Map<String, Object> error(HttpServletRequest request, HttpServletResponse response) {

		Map<String,Object> errorData=getErrorData(request,debug);

		return errorData;
	}
	
	/* (non-Javadoc)
	 * @see com.mauricioBatlle.services.cryptservice.controllers.ErrorControlInterface#getErrorPath()
	 */
	@Override
	public String getErrorPath() {
		return PATH;
	}
	
	private Map<String, Object> getErrorData(HttpServletRequest request, boolean includeStackTrace){
		RequestAttributes reqAttributes = new ServletRequestAttributes(request);
		return errorAttributes.getErrorAttributes(reqAttributes, includeStackTrace);
	}

}
