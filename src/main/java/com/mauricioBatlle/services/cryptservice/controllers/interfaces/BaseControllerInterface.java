package com.mauricioBatlle.services.cryptservice.controllers.interfaces;
import com.mauricioBatlle.services.cryptservice.pojos.requests.CryptoFileRequestPojo;
import com.mauricioBatlle.services.cryptservice.pojos.requests.CryptoMetaFileRequestPojo;
import com.mauricioBatlle.services.cryptservice.pojos.requests.CryptoMetaTextRequestPojo;
import com.mauricioBatlle.services.cryptservice.pojos.requests.CryptoTextRequestPojo;
import com.mauricioBatlle.services.cryptservice.pojos.requests.PlainFileRequestPojo;
import com.mauricioBatlle.services.cryptservice.pojos.requests.PlainTextRequestPojo;
import com.mauricioBatlle.services.cryptservice.pojos.responses.CryptoFileResponsePojo;
import com.mauricioBatlle.services.cryptservice.pojos.responses.CryptoTextResponsePojo;
import com.mauricioBatlle.services.cryptservice.pojos.responses.PlainFileResponsePojo;
import com.mauricioBatlle.services.cryptservice.pojos.responses.PlainTextResponsePojo;
public interface BaseControllerInterface {
	public CryptoFileResponsePojo encryptFilePath(CryptoFileRequestPojo jsonRequest);
	public CryptoTextResponsePojo encryptTextPath(CryptoTextRequestPojo jsonRequest);
	public PlainFileResponsePojo decryptFilePath (PlainFileRequestPojo jsonRequest);
	public PlainTextResponsePojo decryptTextPath (PlainTextRequestPojo jsonRequest);
	CryptoTextResponsePojo encryptMetaTextPath(CryptoMetaTextRequestPojo request);
	CryptoFileResponsePojo encryptMetaFilePath(CryptoMetaFileRequestPojo request);
}
