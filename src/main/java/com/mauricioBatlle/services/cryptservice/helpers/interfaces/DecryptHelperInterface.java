package com.mauricioBatlle.services.cryptservice.helpers.interfaces;
import com.mauricioBatlle.services.cryptservice.pojos.responses.PlainFileResponsePojo;
import com.mauricioBatlle.services.cryptservice.pojos.responses.PlainTextResponsePojo;

public interface DecryptHelperInterface {
public  PlainTextResponsePojo decodeText(String salt, String password, String crc, String cryptCrc,String cryptoText);
public PlainFileResponsePojo decodeBinary(String salt, String password, String crc, String cryptCrc,byte[] cryptoFile);
}
