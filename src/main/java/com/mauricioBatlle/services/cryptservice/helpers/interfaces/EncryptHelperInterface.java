package com.mauricioBatlle.services.cryptservice.helpers.interfaces;

import com.mauricioBatlle.services.cryptservice.pojos.responses.CryptoFileResponsePojo;
import com.mauricioBatlle.services.cryptservice.pojos.responses.CryptoTextResponsePojo;

public interface EncryptHelperInterface {
	public CryptoFileResponsePojo encodeFile(byte[] plainFile);
	public CryptoFileResponsePojo encodeFile(byte[] plainFile, String salt, String password);	
	public CryptoTextResponsePojo encodeText(String plainText);
	public CryptoTextResponsePojo encodeText(String plainText, String salt, String password);
}
