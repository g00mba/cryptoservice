package com.mauricioBatlle.services.cryptservice.helpers;

import java.util.HashMap;
import java.util.Map;
import java.util.zip.CRC32;
import java.util.zip.Checksum;

import org.springframework.security.crypto.encrypt.BytesEncryptor;
import org.springframework.security.crypto.encrypt.Encryptors;
import org.springframework.security.crypto.encrypt.TextEncryptor;
import org.springframework.security.crypto.keygen.KeyGenerators;
import org.springframework.security.crypto.keygen.StringKeyGenerator;
import org.springframework.stereotype.Component;

import com.mauricioBatlle.services.cryptservice.configuration.BadRequestException;
import com.mauricioBatlle.services.cryptservice.configuration.InternalServerErrorException;
import com.mauricioBatlle.services.cryptservice.controllers.CryptoController;
import com.mauricioBatlle.services.cryptservice.helpers.interfaces.*;
import com.mauricioBatlle.services.cryptservice.pojos.responses.CryptoFileResponsePojo;
import com.mauricioBatlle.services.cryptservice.pojos.responses.CryptoTextResponsePojo;
import com.mauricioBatlle.services.cryptservice.pojos.responses.PlainFileResponsePojo;
import com.mauricioBatlle.services.cryptservice.pojos.responses.PlainTextResponsePojo;

import org.slf4j.LoggerFactory;



@Component
public class AppHelper implements DecryptHelperInterface, EncryptHelperInterface, ValidationHelperInterface {
	static final org.slf4j.Logger helperLogger = LoggerFactory.getLogger(CryptoController.class);

	@Override
	public boolean checkCrc(byte[] file, String crc) {
		Checksum check = new CRC32();
		check.update(file,0,file.length);
		String calculated = Long.toHexString(check.getValue());
		boolean response = false;
		if (crc.equals(calculated) ) {
			response = true;
		} else {
			response = false;
		}
		helperLogger.debug("checkCrc file method result: "+response);
		return response;

	}

	@Override
	public boolean checkCrc(String text, String crc) {
		Checksum check = new CRC32();
		check.update(text.getBytes(),0,text.getBytes().length);
		String calculated = Long.toHexString(check.getValue());
		boolean response = false;
		if (crc.equals(calculated)) {
			response = true;
		} else {
			response = false;
		}
		helperLogger.debug("checkCrc text method result: "+response);
		return response;
	}

	@Override
	public CryptoFileResponsePojo encodeFile(byte[] plainFile) {
		helperLogger.info("begin file encryption routine");
		CryptoFileResponsePojo fileResponse = new CryptoFileResponsePojo();

		try {
			Checksum plainCheck = new CRC32();
			plainCheck.update(plainFile,0,plainFile.length);
			String plainFileCrc = Long.toHexString(plainCheck.getValue());

			StringKeyGenerator keyGen = KeyGenerators.string();
			String password = keyGen.generateKey();
			String salt = keyGen.generateKey();

			BytesEncryptor cryptEngine = Encryptors.standard(password, salt);
			byte[] encryptedFile = cryptEngine.encrypt(plainFile);

			plainCheck.reset();
			plainCheck.update(encryptedFile,0,encryptedFile.length);
			String encryptedFileCrc = Long.toHexString(plainCheck.getValue());
			fileResponse.setStatus("200");
			fileResponse.setCrc(plainFileCrc);
			fileResponse.setCryptCrc(encryptedFileCrc);
			fileResponse.setContent(encryptedFile);
			fileResponse.setPassword(password);
			fileResponse.setSalt(salt);
		} catch (Exception e) {
			helperLogger.error("could not encrypt file " + e.getMessage());
			throw new InternalServerErrorException("could not encrypt file " + e.getMessage());
		}
		helperLogger.info("File encription routine terminated ok for file with CRC "+fileResponse.getCrc());
		return fileResponse;
	}

	@Override
	public CryptoTextResponsePojo encodeText(String plainText) {
		helperLogger.info("Begin Text encryption routine");
		CryptoTextResponsePojo textResponse=new CryptoTextResponsePojo();
		try {
			Checksum plainCheck = new CRC32();
			plainCheck.update(plainText.getBytes(),0,plainText.getBytes().length);
			String plainTextCrc = Long.toHexString(plainCheck.getValue());

			StringKeyGenerator keyGen = KeyGenerators.string();
			String password = keyGen.generateKey();
			String salt = keyGen.generateKey();

			TextEncryptor cryptEngine = Encryptors.text(password, salt);
			String encryptedText = cryptEngine.encrypt(plainText);
			plainCheck.reset();
			plainCheck.update(encryptedText.getBytes(),0,encryptedText.getBytes().length);
			String encryptedTextCrc = Long.toHexString(plainCheck.getValue());
			textResponse.setStatus("200");
			textResponse.setCrc(plainTextCrc);
			textResponse.setCryptCrc(encryptedTextCrc);
			textResponse.setContent(encryptedText);
			textResponse.setPassword(password);
			textResponse.setSalt(salt);
		} catch (Exception e) {
			helperLogger.error("could not encrypt text: "+e.getMessage());
			throw new InternalServerErrorException("could not encrypt text: "+e.getMessage());
		}
		helperLogger.info("Text encryption routine finished for text with CRC "+textResponse.getCrc());
		return textResponse;
	}

	@Override
	public PlainTextResponsePojo decodeText(String salt, String password, String crc, String cryptCrc, String cryptoText) {
		helperLogger.info("text decryption routine started");
		Map<String, String> responseMap = new HashMap<>();
		try {
			boolean check=checkCrc(cryptoText, cryptCrc);
			if (check) {
				TextEncryptor cryptoEngine=Encryptors.text(password, salt);
				String decodedText=cryptoEngine.decrypt(cryptoText);
				check=checkCrc(decodedText,crc);

				if (check) {
					responseMap.put("status", "200");
					responseMap.put("text", decodedText);
					helperLogger.info("File successfully decoded");
				}
				else {
					responseMap.put("status", "error");
					String crcCheckFail="Decrypted CRC check does not match original text CRC";
					helperLogger.error(crcCheckFail);
					throw new BadRequestException(crcCheckFail);
				}
				
				
			}
			else {
				responseMap.put("status", "error");
				String fileCrcCheckFail="Decrypted CRC check does not match original encrypted text CRC";
				helperLogger.error(fileCrcCheckFail);
				throw new BadRequestException(fileCrcCheckFail);
			}
		} catch (Exception e) {
				String criticalError="critical error while decoding text: "+e.getMessage();
				helperLogger.error(criticalError);
				throw new InternalServerErrorException(criticalError);
		}
		
		PlainTextResponsePojo response = new PlainTextResponsePojo();
		String status=responseMap.get("status")==null?"undefined status":responseMap.get("status");
		String plainText=responseMap.get("text")==null?"":responseMap.get("text");
		
		
		response.setContent(plainText);
		response.setStatus(status);
		
		helperLogger.info("text decryption routine finalized for encrypted text with crc"+ cryptCrc);
		return response;
	}

	@Override
	public PlainFileResponsePojo decodeBinary(String salt, String password, String crc, String cryptCrc, byte[] cryptoFile) {
		PlainFileResponsePojo response=new PlainFileResponsePojo();
		helperLogger.info("text decryption routine started for encrypted string with CRC "+cryptCrc);


		try {
			boolean check=checkCrc(cryptoFile, cryptCrc);
			if (check) {
				BytesEncryptor cryptoEngine=Encryptors.standard(password, salt);
				byte[] decodedFile=cryptoEngine.decrypt(cryptoFile);
				
				check=checkCrc(decodedFile,crc);

				if (check) {
					response.setContent(decodedFile);
					response.setStatus("200");
					helperLogger.info("File successfully decoded");
				}
				else {
					String mismatchedCrcEx="Decrypted CRC check does not match original text CRC";
					helperLogger.error(mismatchedCrcEx);
					throw new BadRequestException(mismatchedCrcEx);
				}
				
				
			}
			else {
				String mismatchedOrigCrc="Decrypted CRC check does not match original encrypted text CRC";
				helperLogger.error(mismatchedOrigCrc);
				throw new BadRequestException(mismatchedOrigCrc);
			}
		} catch (Exception e) {
				String criticalCryptoEx="critical error while decoding text: "+e.getMessage();
				helperLogger.error(criticalCryptoEx);
				throw new InternalServerErrorException(criticalCryptoEx);
		}
		

		
		helperLogger.info("text decryption routine finalized");
		return response;
	}

	@Override
	public CryptoFileResponsePojo encodeFile(byte[] plainFile, String salt, String password) {
		helperLogger.info("begin file encryption routine");
		CryptoFileResponsePojo fileResponse = new CryptoFileResponsePojo();

		try {
			Checksum plainCheck = new CRC32();
			plainCheck.update(plainFile,0,plainFile.length);
			String plainFileCrc = Long.toHexString(plainCheck.getValue());

			BytesEncryptor cryptEngine = Encryptors.standard(password, salt);
			byte[] encryptedFile = cryptEngine.encrypt(plainFile);

			plainCheck.reset();
			plainCheck.update(encryptedFile,0,encryptedFile.length);
			String encryptedFileCrc = Long.toHexString(plainCheck.getValue());
			fileResponse.setStatus("200");
			fileResponse.setCrc(plainFileCrc);
			fileResponse.setCryptCrc(encryptedFileCrc);
			fileResponse.setContent(encryptedFile);
			fileResponse.setPassword(password);
			fileResponse.setSalt(salt);
		} catch (Exception e) {
			helperLogger.error("could not encrypt file " + e.getMessage());
			throw new InternalServerErrorException("could not encrypt file " + e.getMessage());
		}
		helperLogger.info("File encription routine terminated ok for file with CRC "+fileResponse.getCrc());
		return fileResponse;
	}

	@Override
	public CryptoTextResponsePojo encodeText(String plainText, String salt, String password) {
		helperLogger.info("Begin Text encryption routine");
		helperLogger.info("salt and password provided");
		CryptoTextResponsePojo textResponse=new CryptoTextResponsePojo();
		try {
			Checksum plainCheck = new CRC32();
			plainCheck.update(plainText.getBytes(),0,plainText.getBytes().length);
			String plainTextCrc = Long.toHexString(plainCheck.getValue());

			TextEncryptor cryptEngine = Encryptors.text(password, salt);
			String encryptedText = cryptEngine.encrypt(plainText);
			plainCheck.reset();
			plainCheck.update(encryptedText.getBytes(),0,encryptedText.getBytes().length);
			String encryptedTextCrc = Long.toHexString(plainCheck.getValue());
			textResponse.setStatus("200");
			textResponse.setCrc(plainTextCrc);
			textResponse.setCryptCrc(encryptedTextCrc);
			textResponse.setContent(encryptedText);
			textResponse.setPassword( password);
			textResponse.setSalt(salt);
		} catch (Exception e) {
			helperLogger.error("AppHelper class could not encrypt text: "+e.getMessage());
			throw new InternalServerErrorException("AppHelper class could not encrypt text: "+e.getMessage());
		}
		helperLogger.info("Text encryption routine finished for text with CRC "+textResponse.getCrc());
		return textResponse;
	}

}
