package com.mauricioBatlle.services.cryptservice.helpers.interfaces;

public interface ValidationHelperInterface {
	boolean checkCrc (byte[]file, String crc);
	boolean checkCrc (String text, String crc);
}
