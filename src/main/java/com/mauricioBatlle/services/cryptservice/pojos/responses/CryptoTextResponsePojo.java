package com.mauricioBatlle.services.cryptservice.pojos.responses;
import org.springframework.stereotype.Component;

@Component 
public class CryptoTextResponsePojo {
	private String crc;
	private String cryptCrc;
	private String content;
	private String password;
	private String salt;
	private String status;
	/**
	 * @return the crc
	 */
	public String getCrc() {
		return crc;
	}
	/**
	 * @return the cryptCrc
	 */
	public String getCryptCrc() {
		return cryptCrc;
	}
	/**
	 * @return the content
	 */
	public String getContent() {
		return content;
	}
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * @return the salt
	 */
	public String getSalt() {
		return salt;
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param crc the crc to set
	 */
	public void setCrc(String crc) {
		this.crc = crc;
	}
	/**
	 * @param cryptCrc the cryptCrc to set
	 */
	public void setCryptCrc(String cryptCrc) {
		this.cryptCrc = cryptCrc;
	}
	/**
	 * @param content the content to set
	 */
	public void setContent(String content) {
		this.content = content;
	}
	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	/**
	 * @param salt the salt to set
	 */
	public void setSalt(String salt) {
		this.salt = salt;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
}
