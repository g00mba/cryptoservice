package com.mauricioBatlle.services.cryptservice.pojos.requests;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonProperty;

@Component
public class PlainTextRequestPojo {
	@JsonProperty("content")
	private String textString;
	@JsonProperty("crc")
	private String crc;
	@JsonProperty("salt")
	private String salt;
	@JsonProperty("password")
	private String password;
	@JsonProperty("cryptCrc")
	private String cryptCrc;

	/**
	 * @return the textString
	 */
	public String getTextString() {
		return textString;
	}
	/**
	 * @return the crc
	 */
	public String getCrc() {
		return crc;
	}
	/**
	 * @return the salt
	 */
	public String getSalt() {
		return salt;
	}
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * @return the cryptCrc
	 */
	public String getCryptCrc() {
		return cryptCrc;
	}
	/**
	 * @param textString the textString to set
	 */
	public void setTextString(String textString) {
		this.textString = textString;
	}
	/**
	 * @param crc the crc to set
	 */
	public void setCrc(String crc) {
		this.crc = crc;
	}
	/**
	 * @param salt the salt to set
	 */
	public void setSalt(String salt) {
		this.salt = salt;
	}
	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	/**
	 * @param cryptCrc the cryptCrc to set
	 */
	public void setCryptCrc(String cryptCrc) {
		this.cryptCrc = cryptCrc;
	}

}
