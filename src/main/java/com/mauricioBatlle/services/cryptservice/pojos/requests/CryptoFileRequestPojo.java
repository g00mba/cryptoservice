package com.mauricioBatlle.services.cryptservice.pojos.requests;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonProperty;

@Component
public class CryptoFileRequestPojo {
	@JsonProperty("content")
	private byte[] content;

	/**
	 * @return the content
	 */
	public byte[] getContent() {
		return content;
	}

	/**
	 * @param content the content to set
	 */
	public void setContent(byte[] content) {
		this.content = content;
	}
	
}
