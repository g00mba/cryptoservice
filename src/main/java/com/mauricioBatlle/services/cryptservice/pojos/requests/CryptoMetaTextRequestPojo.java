package com.mauricioBatlle.services.cryptservice.pojos.requests;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonProperty;

@Component
public class CryptoMetaTextRequestPojo {

	@JsonProperty("content")
	private String content;
	@JsonProperty("salt")
	private String salt;
	@JsonProperty("password")
	private String password;
	/**
	 * @return the content
	 */
	public String getContent() {
		return content;
	}
	/**
	 * @return the salt
	 */
	public String getSalt() {
		return salt;
	}
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * @param content the content to set
	 */
	public void setContent(String content) {
		this.content = content;
	}
	/**
	 * @param salt the salt to set
	 */
	public void setSalt(String salt) {
		this.salt = salt;
	}
	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
}
