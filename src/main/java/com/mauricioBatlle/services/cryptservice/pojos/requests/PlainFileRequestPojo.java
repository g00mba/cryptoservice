package com.mauricioBatlle.services.cryptservice.pojos.requests;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PlainFileRequestPojo {
	@JsonProperty("content")
	private byte[] fileString;
	@JsonProperty("crc")
	private String crc;
	@JsonProperty("salt")
	private String salt;
	@JsonProperty("password")
	private String password;
	@JsonProperty("cryptCrc")
	private String cryptCrc;
	/**
	 * @return the fileString
	 */
	public byte[] getFileString() {
		return fileString;
	}
	/**
	 * @return the crc
	 */
	public String getCrc() {
		return crc;
	}
	/**
	 * @return the salt
	 */
	public String getSalt() {
		return salt;
	}
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * @return the cryptCrc
	 */
	public String getCryptCrc() {
		return cryptCrc;
	}
	/**
	 * @param fileString the fileString to set
	 */
	public void setFileString(byte[] fileString) {
		this.fileString = fileString;
	}
	/**
	 * @param crc the crc to set
	 */
	public void setCrc(String crc) {
		this.crc = crc;
	}
	/**
	 * @param salt the salt to set
	 */
	public void setSalt(String salt) {
		this.salt = salt;
	}
	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	/**
	 * @param cryptoCrc the cryptCrc to set
	 */
	public void setCryptCrc(String cryptCrc) {
		this.cryptCrc = cryptCrc;
	}
	

}
