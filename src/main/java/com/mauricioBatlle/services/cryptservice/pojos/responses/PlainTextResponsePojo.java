package com.mauricioBatlle.services.cryptservice.pojos.responses;
import org.springframework.stereotype.Component;

@Component 
public class PlainTextResponsePojo {
	private String content;
	private String status;

	/**
	 * @return the content
	 */
	public String getContent() {
		return content;
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param content the content to set
	 */
	public void setContent(String content) {
		this.content = content;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

}
